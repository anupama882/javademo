package Java8InterfaceChange;

public interface InterfaceChangeJav8 {

    //methods with body for which implementing classes need not to give implementation
    default void defaultMethod()
    {
        System.out.println("It is a default method from InterfaceChangeJav8");
    }


}
