package Java8InterfaceChange;

public interface InterfaceChange {

    //methods with body for which implementing classes need not to give implementation
    default void defaultMethod()
    {
        System.out.println("It is a default method from InterfaceChange");
    }

    //Static methods are also concrete methods but they can’t be implemented.
    static void staticMethod()
    {
        System.out.println("It is a static method");
    }


}
