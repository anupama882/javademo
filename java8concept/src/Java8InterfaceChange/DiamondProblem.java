package Java8InterfaceChange;

public class DiamondProblem implements InterfaceChange, InterfaceChangeJav8 {


    public static void main(String[] args) {

        new DiamondProblem().defaultMethod();

    }

    //must implement
    @Override
    public void defaultMethod()
    {
        InterfaceChange.super.defaultMethod();
    }
}
