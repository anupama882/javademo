package methodreference;

import model.Student;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class OperatorConcept {
    public static void main(String[] args) {

        unaryOPer();

        methodReference();
    }

    public static void unaryOPer() {

        UnaryOperator<Integer> unaryOperator = x -> x * 4;

        System.out.println(unaryOperator.apply(5));

        BinaryOperator<Integer> binaryOperator = Integer::sum;

    }
    // shortened versions of lambda expressions calling a specific method
    public static void methodReference() {
        List<String> list = Arrays.asList("abc", "java", "python");
        list.stream().map(String::toUpperCase).forEach(System.out::println);
    }

}
