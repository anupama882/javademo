package functionalinterface;

import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FunctionConcept {


    public static void main(String[] args) {
        List<Student> studtList = new ArrayList<>();
        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));

        functionWithLoop(studtList);

        functionCombine(studtList);
    }

    public static void functionWithLoop(List<Student> student)
    {
        Function<Student, String> fetchName = Student::getName;

        for (Student s : student) {
                System.out.println(fetchName.apply(s));
             }

    }

    public static void functionCombine(List<Student> student)
    {
        Function<List<Student>, List<String>> fetchNames = list ->
        {
            List<String> nameList = new ArrayList<>();
            list.forEach(s -> nameList.add(s.getName()));
            return nameList;
        };

        Function<List<String>, String> nameFormat = list ->
        {
            StringBuilder sb = new StringBuilder();
            for (String s : list)
                sb.append(s).append(",");
            return sb.toString();
        };

        String name = fetchNames.andThen(nameFormat).apply(student);

        System.out.println(name);

    }
}
