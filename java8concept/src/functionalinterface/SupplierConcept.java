package functionalinterface;

import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class SupplierConcept {

    public static void main(String[] args) {
        List<Student> studtList = new ArrayList<>();
        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));
    }


    public static void supplierWithLoop(List<Student> student) {

        Supplier<Student> createStudentObject = Student::new;
        Student studentObj = createStudentObject.get();


    }
}
