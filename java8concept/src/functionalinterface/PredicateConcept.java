package functionalinterface;

import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateConcept {

    public static void main(String[] args) {

        List<Student> studtList = new ArrayList<>();

        Predicate<Student> verifySubject = sub -> sub.getBranch().equals("cs");

        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));

        predicateWithLoop(studtList);

        predicateWithFilter(studtList);

        predicateBasicHigherOrderFunction(studtList, verifySubject);


    }


    public static List<Student> predicateWithLoop(List<Student> student)
    {
        Predicate<Student> verifySubject = sub -> sub.getBranch().equals("cs");

        List<Student> csStudentList = new ArrayList<>();

        for (Student s : student)
        {
            if (verifySubject.test(s))
                csStudentList.add(s);
        }
        return csStudentList;

    }

    public static void predicateWithFilter(List<Student> student)
    {
        Predicate<Student> verifySubject = sub -> sub.getBranch().equals("cs");

        List<Student> collect = student.stream().filter(verifySubject).collect(Collectors.toList());

        System.out.println(collect);

    }

    public static void predicateBasicHigherOrderFunction(List<Student> student, Predicate<Student> verifySubject) {

        List<Student> collect = student.stream().filter(verifySubject).collect(Collectors.toList());

        for (Student s : collect) {
            System.out.println(s.getBranch() + s.getName() + s.getRollNo());
        }

    }
}
