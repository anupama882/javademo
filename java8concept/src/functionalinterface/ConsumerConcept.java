package functionalinterface;

import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerConcept {
    public static void main(String[] args) {
        List<Student> studtList = new ArrayList<>();

        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));

        consumerWithLoop(studtList);

        combineConsumer(studtList);

    }
    public static void consumerWithLoop(List<Student> student) {
        Consumer<Student> printStudentData = s-> System.out.println
                (s.getRollNo()+s.getName()+s.getBranch());

        for (Student s:student)
        {
            printStudentData.accept(s);
        }

        student.stream().forEach(printStudentData);


    }
    public static void combineConsumer(List<Student> student) {

        Consumer<List<Student>> addCustomer= list->
            list.add(new Student(6,"eva","me"));

        Consumer<List<Student>> printStudentData = l->l.stream().
                forEach( s-> System.out.println(s.getRollNo()+s.getName()+s.getBranch()));

        addCustomer.andThen(printStudentData).accept(student);

        }





    }





