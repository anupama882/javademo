package stream;

import model.Student;

import java.util.*;

public class StreamConcept {
    public static void main(String[] args) {

        streamIntermediateOperation();
        sortMethod();
        terminalOPerations();
    }

    //the stream operations return another new stream
    public static void streamIntermediateOperation() {
        List<String> nameList = Arrays.asList("Dinesh", "Ross", "kagio", "Ross", "lily");

        nameList.stream().filter(s -> s.length() > 5).forEach(System.out::println);
        nameList.stream().map(s -> s.length()).forEach(System.out::println);
        nameList.stream().distinct().forEach(System.out::println);
        nameList.stream().sorted((c1, c2) -> c1.compareTo(c2)).forEach(System.out::println);
        nameList.stream().limit(4).forEach(System.out::println);


    }

    public static void terminalOPerations() {

        List<Integer> list3 = Arrays.asList(1, 2, 3, 4, 56);
        List<Integer> list2 = Arrays.asList(7, 8);
        List<String> nameList = Arrays.asList("Dinesh", "Ross", "kagio", "Ross", "lily");
        nameList.stream().toArray(String[]::new);

        String[] str = nameList.stream().toArray(e -> new String[e]);
        System.out.println(str[0]);

        Integer integer = list2.stream().min((c1, c2) -> c1.compareTo(c2)).orElse(6);
        System.out.println(integer);

        //Returns true if any one element of a stream matches with given predicate
        boolean b = list3.stream().anyMatch(x -> x > 5);
        System.out.println(b);
        Optional<Integer> any = list2.stream().findAny();
        System.out.println(any);


    }

    public static void sortMethod() {

        Comparator<Student> comparing = Comparator.comparing(Student::getRollNo);
      //creating list 

        List<Student> studtList = new ArrayList<>();
        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));

        studtList.stream().sorted(comparing).forEach(StreamConcept::accept);

    }

    private static void accept(Student s) {
        System.out.println(s.getRollNo() + s.getBranch() + s.getName());
    }
}
