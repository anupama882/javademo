package collector;

import model.Student;

import java.util.*;
import java.util.stream.Collectors;

public class CollectorConcept {
    public static void main(String[] args)
    {

        List<Student> studtList = new ArrayList<>();

        studtList.add(new Student(1, "steve", "cs"));
        studtList.add(new Student(2, "don", "ec"));
        studtList.add(new Student(3, "lily", "cs"));
        studtList.add(new Student(4, "emily", "me"));
        studtList.add(new Student(5, "drew", "cs"));

        collectInputToCollections(studtList);
        collectorsAdditionalMethods(studtList);

    }

    public static void collectInputToCollections(List<Student> student) {

        List<Student> collect = student.stream().filter(CollectorConcept::validateBranch).
                collect(Collectors.toList());

        Set<Student> collect1 = student.stream().sorted((s1, s2) -> s1.getName().
                compareTo(s2.getName())).limit(6).collect(Collectors.toSet());

        Map<String, String> collect2 = student.stream().
                collect(Collectors.toMap(Student::getName, Student::getBranch));

    }

    public static void collectorsAdditionalMethods(List<Student> student) {

        IntSummaryStatistics collect = student.stream().
                collect(Collectors.summarizingInt(Student::getRollNo));
        System.out.println(collect.getMax());

        Integer collect1 = student.stream().collect(Collectors.summingInt(Student::getRollNo));
        System.out.println(collect1);

        Map<Boolean, List<Student>> cs = student.stream().
                collect(Collectors.partitioningBy(s -> s.getBranch().equals("cs")));

        Map<String, List<Student>> collect2 = student.stream().
                collect(Collectors.groupingBy(Student::getBranch));

        String collect3 = student.stream().map(Student::getName).
                collect(Collectors.joining(","));
        System.out.println(collect3);

        Optional<Integer> collect4 = student.stream().
                map(Student::getRollNo).collect(Collectors.maxBy((s1, s2) -> s1.compareTo(s2)));


    }

    public static Boolean validateBranch(Student stud) {

        return stud.equals("cs");
    }


}
