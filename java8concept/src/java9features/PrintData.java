package java9features;

public interface PrintData<T> {

    void print(T msg);
}
