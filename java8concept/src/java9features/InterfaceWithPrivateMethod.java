package java9features;

public interface InterfaceWithPrivateMethod {

    private String getData() {
        return "interface private method";
    }


    private static void printData() {
        System.out.println();
    }

    default String check() {
        return getData();
    }

    default String check2() {
        printData();

        InterfaceWithPrivateMethod InterfaceWithPrivateMethod = new InterfaceWithPrivateMethod() {
        };


        return InterfaceWithPrivateMethod.getData();
    }

}
