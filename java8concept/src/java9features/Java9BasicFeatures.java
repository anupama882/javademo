package java9features;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class Java9BasicFeatures {

    public static void main(String[] args) {
        ProcessHandleMethod();
        immutableSet();
        tryWithResourcesJava9();
        anonymousDiamodOperator();

    }

    public static void ProcessHandleMethod() {
        //current method returns an object representing a process of currently running JVM
        ProcessHandle processObject = ProcessHandle.current();

        ProcessHandle.Info info = processObject.info();
        Optional<String[]> arguments = info.arguments();
        arguments.ifPresent((arr) -> System.out.println("parameter"+arr.toString()));

    }


    public static void tryWithResourcesJava7() {
        String message="read the line";
        Reader inputString = new StringReader(message);
        BufferedReader br = new BufferedReader(inputString);
        try (BufferedReader br1 = br) {
            System.out.println(br1.readLine());
        }catch (Exception ex){
           System.out.println(ex);
        }
        // java 9 we don't need to create fresh variable

    }
    public static void tryWithResourcesJava9() {
        String message="read the line";
        Reader inputString = new StringReader(message);
        BufferedReader br = new BufferedReader(inputString);
        try (br) {
            System.out.println(br.readLine());
        }catch (Exception ex){
            System.out.println(ex);
        }

    }
    public static void anonymousDiamodOperator(){
        PrintData<String> printData = new PrintData<String>() {
            @Override
            public void print(String msg) {
              System.out.println(msg);
            }
        };
        printData.print("new diamond operator in java 9 ");
    }
    public static  void immutableSet(){
        Set<String> nameSet = Set.of("lily", "Rose", "Andrew");

        // UnsupportedOperationException
        //nameSet.add("d");
        System.out.println(nameSet);
        List<String> list = List.of();

        System.out.println(list);
        // UnsupportedOperationException
       // list.add("anu");
    }


}
