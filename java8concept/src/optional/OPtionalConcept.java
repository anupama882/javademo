package optional;

import java.util.Optional;

public class OPtionalConcept {

    public static void main(String[] args) {
        optionalMethods();

    }


    static void optionalMethods()
    {
        String name = "anupama";
        Optional<String> empty = Optional.empty();
        System.out.println(empty.isPresent());

        Optional<String> name1 = Optional.of(name);
        System.out.println(name1.isPresent());

        Optional<String> name2 = Optional.ofNullable(name);
        System.out.println(name2.isPresent());
        name2.ifPresent(OPtionalConcept::accept);

        String str = null;

        String optName = Optional.ofNullable(str).orElse("priya");
        System.out.println(optName);


        String s = Optional.ofNullable(str).orElseGet(() -> "null value");
        System.out.println(s);


        String s1 = Optional.ofNullable(str).orElseThrow(IllegalArgumentException::new);

        System.out.println(s1);
    }

    public static void accept(String name) {
        System.out.println(name);
    }
}
