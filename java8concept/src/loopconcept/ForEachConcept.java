package loopconcept;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ForEachConcept {

    public static void main(String[] args) {
        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1, "larry");
        map1.put(2, "steve");
        map1.put(3, "James");
        mapIteration(map1);
        List<String> list = Arrays.asList("abc", "java", "python");
        listIteration(list);
        intStream();

    }


    private static void accept(Integer k, String v) {
        System.out.println();
    }

    private static void accept2(Integer k, String v) {
        if (k != null) {
            System.out.println("Key" + k + "value" + v);
        }

    }

    private static void accept3(String s) {

        StringBuilder s1 = new StringBuilder();

        for (char c : s.toCharArray())
        {
            String hex = Integer.toHexString(c);
            s1.append(hex);
        }

        System.out.println(s + "value is" + s1);

    }

    public static void mapIteration(Map<Integer, String> map) {

        map.forEach((k, v) -> System.out.println("Key" + k + "value" + v));

        //ensure map is not null
        if (map != null) {
            map.forEach(ForEachConcept::accept);
        }
        //don't want to print null key
        map.forEach(ForEachConcept::accept2);

    }

    public static void listIteration(List<String> list) {

        list.forEach(System.out::println);
        list.stream().forEach(System.out::println);

        Consumer<String> print = ForEachConcept::accept3;

        list.forEach(print);

    }

    public static void intStream() {
        String[] name = {"java", "node", "javaScript"};

        List<String> list = IntStream.range(0, name.length).
                mapToObj(index -> index + "value is" + name[index]).collect(Collectors.toList());

        list.forEach(System.out::println);

        // peek()=>Performs an additional action on each element of a stream
        int sum = IntStream.range(0, 10).map(x -> x + 2).limit(6).
                peek(System.out::println).reduce(0, (x, y) -> x + y);

        System.out.println(sum);

        // IntStream.range(0,13);
        List<Integer> collect = IntStream.iterate(2, e -> e * 2).
                limit(10).boxed().collect(Collectors.toList());

        collect.stream().forEach(System.out::println);

    }


}
