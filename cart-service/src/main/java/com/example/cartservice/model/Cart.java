package com.example.cartservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="cartdata")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cartId;
    private Long customerId;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="cart_by_id")
    @OrderColumn(name="type")
    private List<Product> productList ;


}
