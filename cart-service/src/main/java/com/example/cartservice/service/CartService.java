package com.example.cartservice.service;


import com.example.cartservice.exception.DataNotFoundException;
import com.example.cartservice.model.Cart;
import com.example.cartservice.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartService {

    @Autowired
    private CartRepository cartRepository;


    public Cart getCartDataByCustomerId(int customerId) {

        Cart  fetchedCart = cartRepository.findById((long) customerId).
                orElseThrow(() -> new DataNotFoundException("customer not present"));

        return fetchedCart;
    }
    public Cart addCart(Cart cart) {

        return cartRepository.save(cart);

    }


}
