package com.example.cartservice.controller;


import com.example.cartservice.exception.DataNotFoundException;
import com.example.cartservice.model.Cart;
import com.example.cartservice.model.Product;
import com.example.cartservice.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;

@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    CartService cartservice;

    @GetMapping("/data/{id}")
    public ResponseEntity<Cart> getCustomerDataById(@PathVariable int id) {

        try {

            Cart fetchedCart = cartservice.getCartDataByCustomerId(id);
            return new ResponseEntity<Cart>(fetchedCart, HttpStatus.OK);

        } catch (DataNotFoundException dataNotFound) {
            return new ResponseEntity<Cart>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/add")
    public ResponseEntity addCart(@RequestBody Cart cart) {
        try {


            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(cartservice.addCart(cart).getCartId())
                    .toUri();

            return ResponseEntity.created(location).build();

        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }



}





