package com.example.cartservice.repository;

import com.example.cartservice.model.Cart;
import com.example.cartservice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
