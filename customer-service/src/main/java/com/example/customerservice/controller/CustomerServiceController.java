package com.example.customerservice.controller;


import com.example.customerservice.exception.CustomerNotPresentException;
import com.example.customerservice.model.Customer;
import com.example.customerservice.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.Min;
import java.net.URI;
import java.time.Instant;
import java.util.Optional;

@Slf4j
@RestController
@Validated
@RequestMapping("customer")
public class CustomerServiceController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/data/{id}")
    public ResponseEntity<Customer> getCustomerDataById(@PathVariable @Min(1) int id) {

        long startTime = Instant.now().toEpochMilli();

        log.info("fetching customer detail from customer-service for id  {}", id);

        Customer retrievedCustomer = customerService.getCustomerDetailsById(id);

        long endTime = Instant.now().toEpochMilli() - startTime;

        log.info("fetching customer detail from customer-service for id  {} successfully", id);

        log.info("total time {}", endTime);

        return new ResponseEntity<Customer>(retrievedCustomer, HttpStatus.OK);


    }

    @PostMapping("/add")
    public ResponseEntity addCustomer(@RequestBody Customer customer) {


        long startTime = Instant.now().toEpochMilli();

        log.info("adding customer");

        Customer savedCustomer=Optional.ofNullable(customerService.addCustomer(customer)).
                orElseThrow(RuntimeException::new ) ;

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedCustomer.getCustomerId())
                .toUri();

        long endTime = Instant.now().toEpochMilli() - startTime;

        log.info("customer added successfully with id {}", savedCustomer.getCustomerId());

        log.info("total time {}", endTime);
        return ResponseEntity.created(location).build();


    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCustomerById(@PathVariable @Min(1) int id) {


        long startTime = Instant.now().toEpochMilli();

        log.info("deleting customer detail from customer-service for id  {}", id);

        Optional.ofNullable(customerService.deleteCustomer(id)).
                orElseThrow(() -> new CustomerNotPresentException("customer not present"));

        log.info("customer deleted successfully");

        long endTime = Instant.now().toEpochMilli() - startTime;

        log.info("total time {}", endTime);

        return new ResponseEntity<String>(HttpStatus.OK);


    }

    @PutMapping("/update")
    public ResponseEntity updateEntity(@RequestBody Customer customer) {

        long startTime = Instant.now().toEpochMilli();

        log.info("updating customer data");

         Optional.ofNullable(customerService.updateCustomer(customer)).
                orElseThrow(() -> new CustomerNotPresentException("customer not present with id" + customer.getCustomerId()));

        log.info("updated customer data successfully");

        long endTime = Instant.now().toEpochMilli();

        log.info("total time {}", endTime);

        return new ResponseEntity<Customer>(customer, HttpStatus.OK);


    }


}
