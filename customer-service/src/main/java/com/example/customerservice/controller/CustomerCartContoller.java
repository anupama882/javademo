package com.example.customerservice.controller;


import com.example.customerservice.dto.Cart;
import com.example.customerservice.exception.CustomerNotPresentException;
import com.example.customerservice.model.Customer;
import com.example.customerservice.model.CustomerData;
import com.example.customerservice.service.impl.CustomerCartServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.net.URI;
import java.util.Optional;

@Slf4j
@RestController
@Validated
@RequestMapping("customer/cart")
public class CustomerCartContoller {

    @Autowired
    CustomerCartServiceImpl customerCartServiceImpl;

    @GetMapping("/data/{id}")
    public ResponseEntity<Cart> getCustomerDataById(@PathVariable @Min(1) int id) {




        try {
            /*Optional<ResponseEntity<Cart>> cartResponseEntity = Optional.ofNullable(customerCartServiceImpl.getCartDateByCustomerId(id)).
                    filter(res -> !(res.getStatusCode().is4xxClientError())).map((res) ->
                    new ResponseEntity<Cart>(res.getBody(), HttpStatus.OK));*/

            ResponseEntity<Cart> response = customerCartServiceImpl.getCartDateByCustomerId(id);
          return  new ResponseEntity<Cart>(response.getBody(), HttpStatus.OK);

        }catch (Exception exception)   {
            throw new CustomerNotPresentException("customer not found");
        }




    }

    @PostMapping("/add")
    public ResponseEntity addCustomer(@RequestBody Cart cart) {

        try {
            URI location = customerCartServiceImpl.addCartData(cart);
            return ResponseEntity.created(location).build();
        } catch (Exception e) {

            throw e;
        }
    }
}