package com.example.customerservice.controller;

import com.example.customerservice.model.CustomerData;
import com.example.customerservice.service.CustomerDatabaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.Min;
import java.net.URI;
import java.time.Instant;

@Slf4j
@RestController
@Validated
@RequestMapping("customer/database")
public class CustomerServiceDatabaseController {

    @Autowired
    CustomerDatabaseService customerDatabaseService;

    @GetMapping("/data/{id}")
    public ResponseEntity<CustomerData> getCustomerDataById(@PathVariable @Min(1) int id) {
        try {
            long startTime = Instant.now().toEpochMilli();

            log.info("fetching customer detail from customer-service for id  {}", id);

            CustomerData retrievedCustomer = customerDatabaseService.getCustomerDetailsById(id);

            long endTime = Instant.now().toEpochMilli() - startTime;

            log.info("fetching customer detail from customer-service for id  {} successfully", id);

            log.info("total time {}", endTime);

            return new ResponseEntity<CustomerData>(retrievedCustomer, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }

    }

    @PostMapping("/add")
    public ResponseEntity addCustomer(@RequestBody CustomerData customer) {

        try {
            long startTime = Instant.now().toEpochMilli();

            log.info("adding customer");

            CustomerData savedCustomer = customerDatabaseService.addCustomer(customer);

            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(savedCustomer.getCustomerId())
                    .toUri();

            long endTime = Instant.now().toEpochMilli() - startTime;

            log.info("customer added successfully with id {}", savedCustomer.getCustomerId());

            log.info("total time {}", endTime);

            return ResponseEntity.created(location).build();
        } catch (Exception e) {

            throw e;
        }


    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCustomerById(@PathVariable @Min(1) int id) {

        try {
            long startTime = Instant.now().toEpochMilli();

            log.info("deleting customer detail from customer-service for id  {}", id);

            customerDatabaseService.deleteCustomer(id);

            log.info("customer deleted successfully");

            long endTime = Instant.now().toEpochMilli() - startTime;

            log.info("total time {}", endTime);

            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @PutMapping("/update")
    public ResponseEntity updateEntity(@RequestBody CustomerData customer) {

        try {

            long startTime = Instant.now().toEpochMilli();

            log.info("updating customer data");

            customerDatabaseService.updateCustomer(customer);

            log.info("updated customer data successfully");

            long endTime = Instant.now().toEpochMilli();

            log.info("total time {}", endTime);

            return new ResponseEntity<CustomerData>(customer, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }

    }


}
