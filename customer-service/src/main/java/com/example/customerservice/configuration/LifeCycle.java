package com.example.customerservice.configuration;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class LifeCycle {

    @PostConstruct
    public void init(){

        System.out.println("life cycle init method");

    }

    @PreDestroy
    public void preDestroy(){
        System.out.println("pre destroy method ");
    }




}
