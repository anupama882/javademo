package com.example.customerservice.service.impl;

import com.example.customerservice.dto.Cart;
import com.example.customerservice.model.Customer;
import com.example.customerservice.model.CustomerSeedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerCartServiceImpl {

    @Value("${SERVICE_DEMO_SERVICE_HOST:http://localhost:9092/cart}")
    private String baseUrl;

    @Autowired
    private RestTemplate restTemplate;


    public ResponseEntity<Cart> getCartDateByCustomerId(int id) {

      return  restTemplate.getForEntity(baseUrl+"/data/"+ id, Cart.class);


    }

      public  URI  addCartData(Cart cart){

          HttpEntity<Cart> request = new HttpEntity<>(cart);
          return restTemplate
                  .postForLocation(baseUrl+"/add", request);


      }

}
