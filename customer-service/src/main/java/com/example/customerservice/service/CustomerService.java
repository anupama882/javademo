package com.example.customerservice.service;

import com.example.customerservice.model.Customer;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomerDetails();

    Customer getCustomerDetailsById(int id);

    Customer addCustomer(Customer customer);

    Customer deleteCustomer(int id);

    Customer updateCustomer(Customer customerUpdateData);


}
