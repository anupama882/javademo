package com.example.customerservice.service.impl;

import com.example.customerservice.exception.CustomerNotPresentException;
import com.example.customerservice.model.CustomerData;
import com.example.customerservice.repository.CustomerRepository;
import com.example.customerservice.service.CustomerDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerDatabaseServiceImpl implements CustomerDatabaseService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public CustomerData getCustomerDetailsById(int id) {

        Optional<CustomerData> retrievedCustomer = customerRepository.findById(new Long(id));

        retrievedCustomer.orElseThrow(() -> new
                CustomerNotPresentException(" customer not present with id :" + id));

        return retrievedCustomer.get();

    }

    @Override
    public CustomerData addCustomer(CustomerData customer) {

        CustomerData savedCustomer = customerRepository.save(customer);

        return savedCustomer;
    }

    @Override
    public void deleteCustomer(int id) {

        customerRepository.deleteById(Long.valueOf(id));
    }

    @Override
    public CustomerData updateCustomer(CustomerData customerUpdateData) {

        return customerRepository.findById(customerUpdateData.getCustomerId()).map(customer -> {
            customer.setCustomerName(customerUpdateData.getCustomerName());
            customer.setAddress(customerUpdateData.getAddress());

            return customerRepository.save(customer);

        }).orElseThrow(() -> new
                CustomerNotPresentException("customer not present with id :" + customerUpdateData.getCustomerId()));

    }
}
