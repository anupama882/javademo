package com.example.customerservice.service.impl;

import com.example.customerservice.exception.CustomerNotPresentException;
import com.example.customerservice.model.Address;
import com.example.customerservice.model.Customer;
import com.example.customerservice.model.CustomerSeedData;
import com.example.customerservice.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {


    @Override
    public List<Customer> getAllCustomerDetails() {

        return CustomerSeedData.getData().stream()
                .collect(Collectors.toList());

    }

    @Override
    public Customer getCustomerDetailsById(int id) {

        Optional<Customer> first = CustomerSeedData.getData().stream().
                filter(c -> c.getCustomerId() == id).findFirst();

        return first.orElseThrow(() -> new CustomerNotPresentException("customer not present"));
    }


    @Override
    public Customer addCustomer(Customer customer) {

        customer.setCustomerId(CustomerSeedData.getData().size());

        CustomerSeedData.getData().add(customer);

        if ((CustomerSeedData.getData().stream().anyMatch(c -> c.getCustomerId() == customer.getCustomerId()))) {

            return customer;
        }

        return null;
    }


    @Override
    public Customer deleteCustomer(int id) {

        Iterator<Customer> iterator = CustomerSeedData.getData().iterator();

        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            if (customer.getCustomerId() == id) {
                iterator.remove();
                return customer;

            }

        }

        return null;
    }


    public Customer updateCustomer(Customer customerUpdateData) {

        Optional<Customer> first = CustomerSeedData.getData().stream().filter(customer -> customer.getCustomerId() ==
                customerUpdateData.getCustomerId()).findFirst();

        if (first.isPresent()) {
            ListIterator<Customer> iterator = CustomerSeedData.getData().listIterator();
            while (iterator.hasNext()) {
                Customer customer = iterator.next();
                if (customer.getCustomerId() == first.get().getCustomerId()) {
                    iterator.set(customerUpdateData);
                }
                return customerUpdateData;
            }

        }
        return null;
    }


}
