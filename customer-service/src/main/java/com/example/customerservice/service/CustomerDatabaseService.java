package com.example.customerservice.service;

import com.example.customerservice.model.Customer;
import com.example.customerservice.model.CustomerData;

import java.util.List;

public interface CustomerDatabaseService {

    CustomerData getCustomerDetailsById(int id);

    CustomerData addCustomer(CustomerData customer);

    void deleteCustomer(int id);

    CustomerData updateCustomer(CustomerData customerUpdateData);
}
