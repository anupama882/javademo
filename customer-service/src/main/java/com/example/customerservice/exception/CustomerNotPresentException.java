package com.example.customerservice.exception;


public class CustomerNotPresentException extends RuntimeException {


    public CustomerNotPresentException(String message) {
        super(message);
    }


}
