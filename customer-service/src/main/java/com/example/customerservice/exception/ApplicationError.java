package com.example.customerservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class ApplicationError {

    private String message;
    private Date date;
    private String description;

}
