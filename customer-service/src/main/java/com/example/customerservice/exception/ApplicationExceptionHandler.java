package com.example.customerservice.exception;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.Date;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomerNotPresentException.class)
    public ResponseEntity<ApplicationError> customerDataNotPresent(CustomerNotPresentException ex, WebRequest webReq) {

        ApplicationError errors = new ApplicationError(ex.getMessage(), new Date(), webReq.getDescription(false));

        return new ResponseEntity(errors, HttpStatus.NOT_FOUND);
    }

   /* @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApplicationError errors = new ApplicationError(ex.getMessage(), new Date(), ex.getBindingResult().toString());

        return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
    }
*/

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> constraintViolationException(ConstraintViolationException ex, WebRequest webReq) throws IOException {

        ApplicationError errors = new ApplicationError(ex.getMessage(),new Date(),webReq.getDescription(false) );

        return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRunTimeException(RuntimeException ex, WebRequest webReq) throws IOException {

        ApplicationError errors = new ApplicationError(ex.getMessage(),new Date(),webReq.getDescription(false) );

        return new ResponseEntity<>(errors,HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
