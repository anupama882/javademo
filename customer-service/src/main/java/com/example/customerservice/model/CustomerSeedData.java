package com.example.customerservice.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerSeedData {

    private static List<Customer> customerList = new ArrayList<>();

    static {

        customerList.add(new Customer(1, "lily",
                new Address(67, "timesquare", "new york")));
        customerList.add(new Customer(2, "steve",
                new Address(567, "b2 sqauare", "chicago")));
        customerList.add(new Customer(3, "andrew",
                new Address(202, "timesquare", "Houston")));
        customerList.add(new Customer(4, "don",
                new Address(7, "timesquare", "Los Angeles")));
    }

    public static List<Customer> getData() {

        return customerList;
    }


}
