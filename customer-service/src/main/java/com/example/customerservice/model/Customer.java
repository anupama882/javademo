package com.example.customerservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


//using lombok
@Data
//Generates getters for all fields, a useful toString method, and hashCode and equals implementations that check * all non-transient fields. Will also generate setters for all non-final fields, as well as a constructor.”
@AllArgsConstructor//Generates a all-args constructor
@NoArgsConstructor//Generates a no-args constructor
@RequiredArgsConstructor
//@RequiredArgsConstructor creates a constructor with fields which are annotated by @NonNull annotation


public class Customer {

    private int customerId;

    @NonNull
    private String customerName;

    @NonNull
    private Address address ;


}
