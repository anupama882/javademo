package com.example.customerservice.service;

import com.example.customerservice.exception.CustomerNotPresentException;
import com.example.customerservice.model.Address;
import com.example.customerservice.model.Customer;
import com.example.customerservice.model.CustomerSeedData;
import com.example.customerservice.service.impl.CustomerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;


@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "com.example.*")
public class CustomerServiceImplTest {

    private CustomerService customerServiceObject ;

    private  ArrayList<Customer> dummyList = new ArrayList<>();


    @Before
    public void setUp()
    {

       customerServiceObject = new CustomerServiceImpl();

       PowerMockito.mockStatic(CustomerSeedData.class);

        dummyList.add(new Customer(9999, "lily",
                new Address(67, "timesquare", "new york")));
        dummyList.add(new Customer(9, "steve",
                new Address(567, "b2 sqauare", "chicago")));

    }

    @Test
    public void testGetCustomerDataById_whenCustomerExist()
    {

        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);

        Assertions.assertEquals(9999, customerServiceObject.getCustomerDetailsById(9999).
                getCustomerId());
    }

    @Test
    public void testGetCustomerDataById_whenCustomerNotExistThrowException()
    {

        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);

        Assertions.assertThrows(CustomerNotPresentException.class, () -> {
            customerServiceObject.getCustomerDetailsById(3);
        });

    }

    @Test
    public void testAddCustomer_CustomerAddedSuccessfully()
    {

        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);

        Assertions.assertNotNull(customerServiceObject.addCustomer(new Customer( "steve",
                new Address(567, "b2 sqauare", "chicago"))));

    }

    @Test
    public  void testDeleteCustomer_IfCustomerIsPresent()
    {
        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);

        Customer DeletedCustomer = customerServiceObject.deleteCustomer(9999);

        Assertions.assertEquals(9999, DeletedCustomer.getCustomerId());

    }

    @Test
    public  void testDeleteCustomer_IfCustomerNotPresent()
    {
        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);

        Assertions.assertNull(customerServiceObject.deleteCustomer(9998));

    }

    @Test
    public  void testUpdateCustomer_IfCustomerIsPresent(){
        Mockito.when(CustomerSeedData.getData()).thenReturn(dummyList);
        Assertions.assertNotNull(customerServiceObject.updateCustomer(customerServiceObject.
                addCustomer(new Customer( 9999,"steve",
                new Address(567, "b2 sqauare", "chicago")))));



    }


}
